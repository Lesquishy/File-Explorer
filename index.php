<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous">
	<!--script src='./assets/js/jquery-3.1.1.js'></script-->
	<title>Clara Browser</title>
	<link href="assets/css/styles.min.css" rel="stylesheet"/>
	<script src="http://code.jquery.com/jquery-1.11.0.js"></script>
	<script src="assets/js/script.js"></script>
	<script src="assets/js/common.js"></script>
</head>
<body>
	<?php
		include "./assets/php/log/log-home.php";
	?>
	<!-- Alert Start -->
	<div id="alert">
		<div id="display" onclick="hideAlert()">
			Alert
		</div>
	</div>
	<!-- Alert End -->

	<!-- Loading Start -->
	<div id="loading">
		<span id="load">Loading...<br><h3>This may take some time...</h3></span>
		<noscript>This site wont work without Javascript.</noscript>
	</div>
  <!-- Loading End -->

	<div id="backdrop"></div>

	<!-- Report Start -->
	<div class="center">
		<div id="run" class="content" style="width:440px;">
			<br/>
			<span class="title">Report</span>
			<div id="input">
				<hr style="height:1px;border:none;background-color:#ddd;" />
			<br/>
			<br/>
			<form action="./assets/php/log/log-report.php" method="post">
				<div class="input-group left" style="width:400px;">
					Report bug Here:
				</div>
				<br/>
				<div class="input-group left" style="width:400px;">
					<span class="input-group-addon" id="basic-addon1" style="width:10em;">Error:</span>
					<input name="report" type="text" class="form-control" placeholder="John Wick 2" aria-describedby="basic-addon1"/>
				</div>
				<br/>
				<!--div class="input-group left" style="width:400px;">
					<span class="input-group-addon" id="basic-addon1" style="width:10em;">Username:</span>
					<input type="text" class="form-control" placeholder="server_Demo" aria-describedby="basic-addon1"/>
				</div>
				<br/>
				<div class="input-group left" style="width:400px;">
					<span class="input-group-addon" id="basic-addon1" style="width:10em;">Password:</span>
					<input type="password" class="form-control" placeholder="******" aria-describedby="basic-addon1"/>
				</div-->
				</div>
				<br>
				<button type="submit" class="btn btn-info" id="save">Submit</button>
				<button type="button" class="btn btn-danger" id="cancel">Cancel</button>
			</form>
		</div>
	</div>
	<!-- Report End -->

	<!-- Buttons Start -->
	<p style="color:white; font-size:1.2em;">All systems: <a style="text-decoration:none;" id="return">Error</a></p>
	<table>
			<tr>
				<td>

				</td>
				<td>
					<a id="download" class='btn btn-default' onclick="downloads()"><image src='./assets/images/download.svg'></image> Download</a>
				</td>
			</tr>
		</table>
	<!-- Buttons End -->


	<div class="filemanager">

		<!-- Search Start -->
		<div class="search" id="searchcontain">
			<input type="search" id='searchBox' placeholder="Find a file.."/>
			<image id='searchMark' src='./assets/images/search.svg'></image>
		</div>
		<!-- Search End -->

		<div class="breadcrumbs"></div>

		<div class="nothingfound">
			<div class="nofiles"></div>
			<span>No files here.</span>
		</div>

		<ul class="data"></ul>

	</div>
	<div id="footer"><a onclick="unhide()">Report</a></div>
</body>
</html>
