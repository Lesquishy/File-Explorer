<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Please wait...</title>

  <style>
    *{
      padding:0;
      margin:0;
    }
    html{
      height:100%;
      background-color:#23232e;
      color:white;
      font-weight: bold;
    }
  </style>
</head>
<body>
  <h3>Something has gone wrong... Please wait.</h3>
  <?php
  $pass = true;
  $from = "Bug-Report";
  $extra = $_POST['report'];

  $ip = $_SERVER['REMOTE_ADDR'];
  $log = fopen("../../../files/logs/log.html", "a") or die("Unable to open Log!");
  date_default_timezone_set("Pacific/Auckland");

  if($pass == "true"){
    $status = "<b style='color:orange;'>Report</b>";
  } else {
    $status = "<b style='color:red;'>Failed</b>";
  }
  fwrite($log, "<tr><td>[ " . dates() . " ] </td><td> [ " . $ip . " ] </td><td> [ " . $from . " ] </td><td> [ " . $status . " ]</td><td>[ " . $extra . " ]</td></tr>" );
  fclose($log);
  header("Location: ../../../");

  function dates(){
    return date("d-m-Y") . " ] </td><td> [ " . date("h:i:sa");
  }

  ?>
</body>
</html>
