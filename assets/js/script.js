
$(document).ready(function() {
  var files;
  ajaxCheck();
  sizing();
  clickable();
});

function sizing(){
  var height = Math.round( ( $("body").height() / 100 ) * 76 );
  $(".filemanager .data").css({"height":height})
}

function alert(from, text, color){
  var type = "Warning ";
  var opacity = 0.8;
  if (color == "green"){
    color = "rgba(28,175,57," + opacity + ")";
    type = "Success ";
  } else if(color == "red"){
    color = "rgba(150,3,7," + opacity + ")";
    type = "Error ";
  } else {
    color = "rgba(0,102,204," + opacity + ")";
    type = "Warning ";
  }
  text = type + "From - " + from + " :  '" + text + "'";
  $("#display").text(text);
  $("#alert").css({"display" : "block", "background-color" : color});
  setTimeout(function(){hideAlert()}, 8000);
}

function hideAlert(){
  $("#alert").fadeOut(800);
  setTimeout(function(){$("#display").text("Hide");}, 900);
}

function unhide(){
  $("#head").css({"z-index":"-2"});
  $("#run").css({"opacity":"0", "display":"inline-block"});
  $("#run").animate({opacity:1},500);
  $("#hider").animate({opacity:.4, "z-index":800},400);
  $("#hider").show();
  $("#serverName").html("Sabrina");
  $("#backdrop").fadeIn(400);
}

function clickable(){
  //expanding server
  $("#expand").click(function() {alert("Settings", "This Is Dissabled", "red")})
  /*$("#expand").click(function(evt){
    evt.stopImmediatePropagation();
    evt.preventDefault();
    if($($("#expand").children().get(0)).html() == "&gt;"){
    $("#run").animate({width:"1000px", "max-width":"1000px", height:"440px"});
    window.setTimeout(function(){
      $("#inputXpand").css({"display":"block"});
      $($("#expand").children().get(0)).html("<");
      window.setTimeout(function(){
        $(".input-group.left").animate({width:"450px"});
      }, 400);
        $("#expand").animate({height:"440px"});
    }, 100)
    } else{
      $("#run").animate({width:"470px", "max-width":"800px", height:"340"});
      $("#inputXpand").hide(500);
      $($("#expand").children().get(0)).html(">");
      $(".input-group.left").animate({width:"400px"});
      $("#expand").animate({height:"340px"});
    }
  });*/

  $(".all").click(function() {
    $(".runable").each(function() {
      $(".btn").each(function() {
        if ($(".all>.selected").html() != "Enabled"){
          $(".true").addClass("selected");
          $(".false").removeClass("selected");
        } else{
          $(".true").removeClass("selected");
          $(".false").addClass("selected")
        }
      });
    });
  });

  $(".btn-group").click(function(evt){
    evt.stopImmediatePropagation();
    evt.preventDefault();
    $(this).children().toggleClass("selected");
  });

  $("#save").click(function(){
    if($("#save").html() == "Save"){
      $("#save").toggleClass("btn-info");
      $("#save").toggleClass("btn-success");
      $("#save").html("Saved");
      window.setTimeout(function(){
        $("#save").html("Save");
        $("#save").toggleClass("btn-info");
        $("#save").toggleClass("btn-success");
      }, 2000)
    }
  });

  $("#backdrop").click(function(){
    $("#cancel").trigger("click");
  })

  $("#cancel").click(function(){
    $("#hider").animate({opacity:0, display:"none", "z-index":"-1"});
    $("#run").hide({opacity:0});
    $("#backdrop").fadeOut({opacity:0});
  });

  $(".btn-group").click(function(evt){
    evt.stopImmediatePropagation();
    evt.preventDefault();
    $(this).children().toggleClass("selected");
  });
}

function ajaxCheck(){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     $("#return").text(this.responseText);
    }
  };
  xhttp.open("GET", "./assets/data/return.data");
  xhttp.send();
  if(!$("#return").text() == "Error"){
    console.log("Error Ajax with Request");
  } else {
    $("#return").css({"color":"lime"});
  }
}

function load(state){
  if (state == "start"){
    $('#loading').fadeIn(600);
  } else if (state == "stop"){
    $('#loading').fadeOut(600);
  } else {console.log("Load only accepts Start & Stop as Input")}
}

function downloads(){
  files = tickboxes();
  console.log(files)
  if( isset(files) ){
    $.ajax({
      data: "files=" +files,
      type: "POST",
      url: "./assets/php/download.php",
      success: function(r) {
        console.log("Download Passed Successfully");
        console.log(r);
        load("start");
        setTimeout(function(){
          window.location.href = "./assets/temp/download.zip";
          load("stop");
        }, 2000);
      }
    });
  } else {
    alert("Download", "No Files Selected", "red");
  }
}

function tickboxes(){
  var ticked;
  for(a = 0; $(".selectable:checked").length > a; a++ ){
    if(ticked == undefined){
      ticked = $($(".selectable:checked").get(a)).attr('name');
    } else {
      ticked = ticked + ";" + $($(".selectable:checked").get(a)).attr('name');
    }
  }
  return ticked;
}

function disable(){
	$('.btn').attr('disabled', true)
}

$(function(){
	var filemanager = $('.filemanager'),
		breadcrumbs = $('.breadcrumbs'),
		fileList = filemanager.find('.data');
	// Start by fetching the file data from scan.php with an AJAX request
	$.get('scan.php', function(data) {
		var response = [data],
			currentPath = '',
			breadcrumbsUrls = [];
		var folders = [],
			files = [];
		// This event listener monitors changes on the URL. We use it to
		// capture back/forward navigation in the browser.
		$(window).on('hashchange', function(){
			goto(window.location.hash);
			// We are triggering the event. This will execute
			// this function on page load, so that we show the correct folder:
		}).trigger('hashchange');

		// Hiding and showing the search box
		filemanager.find('.search').click(function(){
			var search = $(this);
			search.find('span').hide();
			search.find('input[type=search]').show().focus();
		});

		// Listening for keyboard input on the search field.
		// We are using the "input" event which detects cut and paste
		// in addition to keyboard input.
		filemanager.find('input').on('input', function(e){
			folders = [];
			files = [];
			var value = this.value.trim();
			if(value.length) {
				filemanager.addClass('searching');
				// Update the hash on every key stroke
				window.location.hash = 'search=' + value.trim();
			}
			else {
				filemanager.removeClass('searching');
				window.location.hash = encodeURIComponent(currentPath);
			}
		}).on('keyup', function(e){
			// Clicking 'ESC' button triggers focusout and cancels the search
			var search = $(this);
			if(e.keyCode == 27) {
				search.trigger('focusout');
			}
		}).focusout(function(e){
			// Cancel the search
			var search = $(this);
			if(!search.val().trim().length) {
				window.location.hash = encodeURIComponent(currentPath);
				search.hide();
				search.parent().find('span').show();
			}
		});

		// Clicking on folders
		fileList.on('click', 'li.folders', function(e){
			e.preventDefault();
			var nextDir = $(this).find('a.folders').attr('href');
			if(filemanager.hasClass('searching')) {
				// Building the breadcrumbs
				breadcrumbsUrls = generateBreadcrumbs(nextDir);
				filemanager.removeClass('searching');
				filemanager.find('input[type=search]').val('').hide();
				filemanager.find('span').show();
			}
			else {
				breadcrumbsUrls.push(nextDir);
			}
			window.location.hash = encodeURIComponent(nextDir);
			currentPath = nextDir;
		});
		// Clicking on breadcrumbs
		breadcrumbs.on('click', 'a', function(e){
			e.preventDefault();
			var index = breadcrumbs.find('a').index($(this)),
				nextDir = breadcrumbsUrls[index];
			breadcrumbsUrls.length = Number(index);
			window.location.hash = encodeURIComponent(nextDir);
		});

		// Navigates to the given hash (path)
		function goto(hash) {
			hash = decodeURIComponent(hash).slice(1).split('=');
			if (hash.length) {
				var rendered = '';
				// if hash has search in it
				if (hash[0] === 'search') {
					filemanager.addClass('searching');
					rendered = searchData(response, hash[1].toLowerCase());
					if (rendered.length) {
						currentPath = hash[0];
						render(rendered);
					}
					else {
						render(rendered);
					}
				}

				// if hash is some path
				else if (hash[0].trim().length) {
					rendered = searchByPath(hash[0]);
					if (rendered.length) {
						currentPath = hash[0];
						breadcrumbsUrls = generateBreadcrumbs(hash[0]);
						render(rendered);
					}
					else {
						currentPath = hash[0];
						breadcrumbsUrls = generateBreadcrumbs(hash[0]);
						render(rendered);
					}
				}
				// if there is no hash
				else {
					currentPath = data.path;
					breadcrumbsUrls.push(data.path);
					render(searchByPath(data.path));
				}
			}
			$('#location').attr('value', currentPath);
      setCookie('location', currentPath, '1')
		}

		// Splits a file path and turns it into clickable breadcrumbs
		function generateBreadcrumbs(nextDir){
			var path = nextDir.split('/').slice(0);
			for(var i=1;i<path.length;i++){
				path[i] = path[i-1]+ '/' +path[i];
			}
			return path;
		}

		// Locates a file by path
		function searchByPath(dir) {
			var path = dir.split('/'),
				demo = response,
				flag = 0;
			for(var i=0;i<path.length;i++){
				for(var j=0;j<demo.length;j++){
					if(demo[j].name === path[i]){
						flag = 1;
						demo = demo[j].items;
						break;
					}
				}
			}
			demo = flag ? demo : [];
			return demo;
		}

		// Recursively search through the file tree
		function searchData(data, searchTerms) {
			data.forEach(function(d){
				if(d.type === 'folder') {
					searchData(d.items,searchTerms);
					if(d.name.toLowerCase().match(searchTerms)) {
						folders.push(d);
					}
				}
				else if(d.type === 'file') {
					if(d.name.toLowerCase().match(searchTerms)) {
						files.push(d);
					}
				}
			});
			return {folders: folders, files: files};
		}

		// Render the HTML for the file manager
		function render(data) {
			var scannedFolders = [],
				scannedFiles = [];
			if(Array.isArray(data)) {
				data.forEach(function (d) {
					if (d.type === 'folder') {
						scannedFolders.push(d);
					}
					else if (d.type === 'file') {
						scannedFiles.push(d);
					}
				});
			}
			else if(typeof data === 'object') {
				scannedFolders = data.folders;
				scannedFiles = data.files;
			}
			// Empty the old result and make the new one
			fileList.empty().hide();
			if(!scannedFolders.length && !scannedFiles.length) {
				filemanager.find('.nothingfound').show();
			}
			else {
				filemanager.find('.nothingfound').hide();
			}
			if(scannedFolders.length) {
				scannedFolders.forEach(function(f) {
					var itemsLength = f.items.length,
						name = escapeHTML(f.name),
						icon = '<span class="icon folder"></span>';
					if(itemsLength) {
						icon = '<span class="icon folder full"></span>';
					}
					if(itemsLength == 1) {
						itemsLength += ' item';
					}
					else if(itemsLength > 1) {
						itemsLength += ' items';
					}
					else {
						itemsLength = 'Empty';
					}
					var folder = $('<li class="folders"><a href="'+ f.path +'" title="'+ f.path +'" class="folders">'+icon+'<span class="name">' + name + '</span> <span class="details">' + itemsLength + '</span></a></li>');
          folder.appendTo(fileList);
				});
			}
			if(scannedFiles.length) {
				scannedFiles.forEach(function(f) {
          f.name = f.name.replace("_", " ")
					var fileSize = bytesToSize(f.size),
						name = escapeHTML(f.name),
						fileType = name.split('.'),
						icon = '<span class="icon file"></span>';
					fileType = fileType[fileType.length-1];
					if (fileType == "jpg") {
						icon = '<div style="display:inline-block;margin:20px 30px 0px 25px;border-radius:8px;width:60px;height:70px;background-position: center center;background-size: cover; background-repeat:no-repeat;background-image: url(' + f.path + ');"></div>';
					} else {
						icon = '<span class="icon file f-'+fileType+'">.'+fileType+'</span>';
					}
					var file = $('<li class="files"><input type="checkbox" class="selectable" name="' + f.path + '"/><a href="'+ f.path+'" title="'+ f.path +'" class="files">'+icon+'<span class="name">'+ name +'</span> <span class="details">'+fileSize+'</span></a></li>');
					file.appendTo(fileList);
				});
			}
			// Generate the breadcrumbs
			var url = '';
			if(filemanager.hasClass('searching')){
				url = '<span>Search results: </span>';
				fileList.removeClass('animated');
			}
			else {
				fileList.addClass('animated');
				breadcrumbsUrls.forEach(function (u, i) {
					var name = u.split('/');
					if (i !== breadcrumbsUrls.length - 1) {
						url += '<a href="'+u+'"><span class="folderName">' + name[name.length-1] + '</span></a> <span class="arrow">→</span> ';
					}
					else {
						url += '<span class="folderName">' + name[name.length-1] + '</span>';
					}
				});
			}
			breadcrumbs.text('').append(url);
			// Show the generated elements
			fileList.animate({'display':'inline-block'});
      $('#loading').fadeOut(600);
		}

		// This function escapes special html characters in names
		function escapeHTML(text) {
			return text.replace(/\&/g,'&amp;').replace(/\</g,'&lt;').replace(/\>/g,'&gt;');
		}

		// Convert file sizes from bytes to human readable units
		function bytesToSize(bytes) {
			var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
			if (bytes == 0) return '0 Bytes';
			var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
			return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
		}
	});
});
