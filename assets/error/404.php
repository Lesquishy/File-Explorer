<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>404</title>
  <style>
    html{
      width:100%;
      height:100%;
      padding:0;
      margin:0;
    }
    body{
      width:100%;
      height:100%;
      padding:0;
      margin:0;
      background-repeat: no-repeat;
      background-image: url("../images/404.jpg");
      background-position: center;
      background-size: cover;
      text-align: center;
      color:white;
      font: 14px normal Arial, Helvetica, sans-serif;
    }
    p{
      font-size:.7em;
    }
  </style>
</head>
<body>
<h1>Sorry ¯\_(ツ)_/¯<h1>
<p>Please go back to the home page. <br> Click 'Settings' Then report what happened</p>
</body>
</html>
